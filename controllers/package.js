const fs = require('fs');

const packageRouter = require('express').Router();
const { getPackageListObj } = require('../utils/helpers');

const debianPackagePath = '/var/lib/dpkg/status';
const mockDataPath = './status.real';

packageRouter.get('/', (request, response) => {
  let file;
  try {
    file = fs.readFileSync(debianPackagePath);
  } catch (e) {
    console.error(`Using mockdata as ${debianPackagePath} file not in your device`);
    file = fs.readFileSync(mockDataPath);
  }
  const packageList = getPackageListObj(file);
  response.json(Object.values(packageList));
});

module.exports = packageRouter;
