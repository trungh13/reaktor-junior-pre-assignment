import React from 'react';

const PackageList = React.memo(
  ({ packageNameList, packageList, currentPkgName, setCurrentPkgName }) => {
    return (
      <>
        {packageNameList.map(packageName => (
          <div key={packageName}>
            <input
              type="radio"
              value={packageName}
              name={packageName}
              checked={currentPkgName === packageName}
              id={packageName + '-radio'}
              onChange={event => setCurrentPkgName(event.target.value)}
            />
            <label htmlFor={packageName + '-radio'} id={packageName}>
              {packageName}
            </label>
          </div>
        ))}
      </>
    );
  }
);

export default PackageList;
