import React from 'react';

const PackageInformation = ({ currentPkg, setCurrentPkgName }) =>
  typeof currentPkg === 'object' && (
    <>
      <ul>
        <li>
          <b>Name:</b> {currentPkg.name}
        </li>
        {currentPkg.description && (
          <li>
            <b>Description:</b> {currentPkg.description}
          </li>
        )}
        {currentPkg.dependents && (
          <li>
            <b>Dependents:</b>
            <ul>
              {currentPkg.dependents.map(dependent => (
                <li key={dependent}>
                  <a href={`#${dependent}`} onClick={() => setCurrentPkgName(dependent)}>
                    {dependent}
                  </a>
                </li>
              ))}
            </ul>
          </li>
        )}
        {currentPkg.dependencies && (
          <li>
            Dependencies:
            <ul>
              {currentPkg.dependencies.map(dependency => (
                <li key={dependency}>
                  <a href={`#${dependency}`} onClick={() => setCurrentPkgName(dependency)}>
                    {dependency}
                  </a>
                </li>
              ))}
            </ul>
          </li>
        )}
      </ul>
    </>
  );

export default PackageInformation;
