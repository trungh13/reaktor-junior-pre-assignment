import React, { useState, useEffect } from "react";
import axios from "axios";

import { arrayToObject, arrayOfNameList } from "./utils/helpers";

import PackageList from "./components/PackageList";
import PackageInformation from "./components/PackageInformation";

const App = React.memo(() => {
  const [packageList, setPackageList] = useState({});
  const [packageNameList, setPackageNameList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPkgName, setCurrentPkgName] = useState("");
  useEffect(() => {
    axios
      .get("/api/packages")
      .then(res => res.data)
      .then(res => {
        setLoading(false);
        setPackageList(arrayToObject(res));
        const arrayNameList = arrayOfNameList(res);
        setPackageNameList(arrayNameList);
        setCurrentPkgName(
          window.location.hash
            ? window.location.hash.replace(/#/, "")
            : arrayNameList[0]
        );
      });
  }, []);

  return loading ? (
    <p>Loading...</p>
  ) : (
    <div className="App">
      <h2 className="title">Package list</h2>
      <div className="package-table">
        <div className="package-list">
          <PackageList
            packageNameList={packageNameList}
            packageList={packageList}
            currentPkgName={currentPkgName}
            setCurrentPkgName={setCurrentPkgName}
          />
        </div>
        <div className="package-information">
          <PackageInformation
            currentPkg={packageList[currentPkgName]}
            setCurrentPkgName={setCurrentPkgName}
          />
        </div>
      </div>
    </div>
  );
});

export default App;
