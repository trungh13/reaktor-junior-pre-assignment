const arrayToObject = arr =>
  arr.reduce((obj, currPkg) => {
    obj[currPkg.name] = currPkg;
    return obj;
  }, {});

const arrayOfNameList = arr =>
  arr.map(({ name }) => name).sort((a, b) => a.localeCompare(b));

module.exports = {
  arrayToObject,
  arrayOfNameList
};
