const fs = require('fs');

const legitPackageKeys = ['Package', 'Depends', 'Description'];

const splitFileToPackageString = file => {
  return file
    .toString()
    .split(/\n\n/)
    .filter(pkg => pkg !== '');
};

const formatPackageToObj = text => {
  const pkg = {};
  let prevKey = '';
  let prevIndex = 0;

  text.split(/\n/).forEach((line, index) => {
    const [key, value] = line.split(/: /);
    if (legitPackageKeys.indexOf(key) !== -1) {
      prevIndex = index;
      if (key === 'Depends') {
        pkg.dependents = value
          .split(/,|\|/)
          .map(element => element.replace(/ *\([^)]*\) *| /g, ''))
          .filter((val, idx, arr) => arr.indexOf(val) === idx);
        prevKey = 'dependents';
      } else {
        prevKey = key === 'Package' ? 'name' : 'description';
        pkg[prevKey] = value;
      }
    } else if (typeof value === 'undefined' && index === prevIndex + 1 && line.indexOf(' ') === 0) {
      pkg[prevKey] += key;
      prevIndex = index;
    }
  });
  return pkg;
};

const formatPackageListToObj = packageListInString => {
  return packageListInString.reduce((obj, currPackage) => {
    const pkg = formatPackageToObj(currPackage);
    const { dependents, name } = pkg;
    if (dependents) {
      dependents.forEach(dependency => {
        if (typeof obj[dependency] === 'object') {
          obj[dependency].dependencies = obj[dependency].dependencies
            ? [...obj[dependency].dependencies, name]
            : [name];
        }
      });
    }
    return { ...obj, [name]: { ...pkg } };
  }, {});
};

const getPackageListObj = file => {
  const packageListInString = splitFileToPackageString(file);
  return formatPackageListToObj(packageListInString);
};

module.exports = {
  getPackageListObj
};
