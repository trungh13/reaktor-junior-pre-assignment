const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const packageRouter = require('./controllers/package');

const middleware = require('./utils/middleware.js');

app.use(cors());
app.use(express.static('build'));
app.use(bodyParser.json());

app.use('/api/packages', packageRouter);

app.use(middleware.unknownEndpoint);
app.use(middleware.errorHandler);

module.exports = app;
