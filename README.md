Live project in [heroku](https://cryptic-mountain-55586.herokuapp.com/).
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

**You’ll need to have Node >= 6 on your local development machine** (but it’s not required on the server). You can use [nvm](https://github.com/creationix/nvm#installation) (macOS/Linux) or [nvm-windows](https://github.com/coreybutler/nvm-windows#node-version-manager-nvm-for-windows) to easily switch Node versions between different projects.

### Installing

A step by step series of examples that tell you how to get a development env running

```
git clone https://gitlab.com/trungh13/reaktor-junior-pre-assignment.git

cd reaktor-junior-pre-assignment

npm install
```

### NPM scripts
All build tasks are configured as NPM scripts so you can execute any relevant task via `npm run [task]`. `Task` list: 
- `start`: Start the NodeJS server.
- `watch`: Start NodeJS server with hot reloading by Nodemon.
- `build:ui`: Remove `build/` folder in root folder. Build ReactJS app and copy `build/` to root folder.
- `heroku:deploy`: Push commits to heroku repo.
- `heroku:deployUi` : Basically build ReactJS app and push it to heroku repo.
- `heroku:logs`: Show live logs in heroku.
- `format`: Lint then format files(js,jsx,json,css,md) with Prettier.
---
©2019 Trung Hoang. Visit me on <a href="https://github.com/trungh13/">Github</a> 